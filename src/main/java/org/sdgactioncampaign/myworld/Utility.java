/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sdgactioncampaign.myworld;

import io.vertx.core.Future;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.redis.RedisClient;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author dipo
 */
public class Utility {

    public static RedisClient redis = null;
    public static LocalMap<String, Object> localMap = null;
    public static HttpClient httpClient = null;
    public static JsonObject config = null;

    public static Map<String, String> splitQuery(String query) throws UnsupportedEncodingException {
        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
        }
        return query_pairs;
    }

    public Future<String> processSurveys(Map surveyMap) {

        Future future = Future.future();
        String[] surveyArray = ((String) surveyMap.get("Body")).split("#");
        IntStream.range(0, surveyArray.length).boxed().forEach((index) -> {
            String survey = surveyArray[index];

            String qString = convertSurveysToQueryString(survey, surveyMap, Utility.config);

            Utility.redis.lpush("surveys", qString, hndlr -> {
                if (hndlr.succeeded()) {

                }
                if (hndlr.failed()) {
                }

                if (index == surveyArray.length - 1) {
                    future.complete();
                }
            });

        });
        return future;
    }

    public static void popFromListToApi() {
        Utility.redis.rpop("surveys", hndlrz -> {
            if (hndlrz.succeeded()) {

                String popped = hndlrz.result();
                if (popped == null) {
                    Utility.localMap.put("dispatch_busy", 0);
                    return;
                }

                System.out.println(popped);

                String url = Utility.config.getString("apiUrl") + "?" + popped;
                System.out.println(url);

                Utility.httpClient.getNow(url, hx -> {
                    System.out.println("sent to api");
                    System.out.println(hx.statusCode() + ":" + hx.statusMessage());
                    if (hx.statusCode() != 200) {
                        Utility.redis.lpush("surveys", popped, b -> {

                        });
                    }
                });
                checkSurveyLength().setHandler(length -> {
                    if (length.succeeded() && length.result() > 0) {
                        popFromListToApi();
                    } else {
                        Utility.localMap.put("dispatch_busy", 0);
                    }
                });

            }

            if (hndlrz.failed()) {
                Utility.localMap.put("dispatch_busy", 0);
            }
        });

    }

    public static Future<Long> checkSurveyLength() {
        Future<Long> future = Future.future();
        Utility.redis.llen("surveys", len -> {
            if (len.succeeded()) {
                future.complete(len.result());
            }
        });
        return future;
    }

    public static String convertSurveysToQueryString(String survey, Map surveyMap, JsonObject config) {
        String q1 = String.valueOf(survey.charAt(0));
        String payload = survey.substring(1);

        String patternString1 = "(\\w)(\\d)+";
        Pattern pattern = Pattern.compile(patternString1);
        Matcher matcher = pattern.matcher(payload);

        JsonObject resJson = config.getJsonObject("apiParam");
        resJson.put("q1", config.getJsonObject("yesNoMap").getString(q1));
        resJson.put("country", (String) surveyMap.get("FromCountry"));
        resJson.put("phone_number", (String) surveyMap.get("From"));

        while (matcher.find()) {
            String choice = matcher.group();
            String goal = config.getJsonObject("goalMap").getString(choice.substring(0, 1));
            int weight = Integer.parseInt(choice.substring(1));
            resJson.put(goal, weight);

        }

        String qString = resJson.stream().map((k) -> {
            return k.getKey() + "=" + k.getValue();
        }).collect(Collectors.joining("&"));
        return qString;

    }

    public static JsonObject getConfiguration(File config) {
        JsonObject conf = new JsonObject();
        if (config.isFile()) {
            System.out.println("Reading config file: " + config.getAbsolutePath());
            try (Scanner scanner = new Scanner(config).useDelimiter("\\A")) {
                String sconf = scanner.next();
                try {
                    conf = new JsonObject(sconf);
                } catch (DecodeException e) {
                    System.err.println("Configuration file " + sconf + " does not contain a valid JSON object");
                }
            } catch (FileNotFoundException e) {
                // Ignore it.
            }
        } else {
            System.out.println("Config file not found " + config.getAbsolutePath());
        }
        return conf;
    }
}
