package org.sdgactioncampaign.myworld;

import com.diabolicallabs.vertx.cron.CronObservable;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.redis.RedisOptions;
import io.vertx.redis.RedisClient;
import io.vertx.rx.java.RxHelper;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.sdgactioncampaign.myworld.endpoints.Endpoints;
import rx.Scheduler;

public class MainVerticle extends AbstractVerticle {

    @Override
    public void start() {

        //========== Router ===========================
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());

        router.get("/").handler(req -> req.response().end("My world Server API"));
        router.post("/surveys").handler(new Endpoints()::handleAddSurvey);

        //========== Redis Server =====================
        Utility.redis = RedisClient.create(vertx, new RedisOptions());

        //========== HTTP Server, Config and Client ===
        File conf = new File("src/conf/config.json");
        if (config() == null) {
            config().mergeIn(Utility.getConfiguration(conf));
        }
        Utility.config = config();
        Utility.httpClient = vertx.createHttpClient();

        //========== Local Map ========================
        SharedData sd = vertx.sharedData();

        Utility.localMap = sd.<String, Object>getLocalMap("localAppMap");
        Utility.localMap.put("dispatch_busy", 0);

        //========== Cron Observable ==================
        Scheduler scheduler = RxHelper.scheduler(vertx);
        CronObservable.cronspec(scheduler, "*/5 * * * * ?", "Africa/Lagos")
                .subscribe(
                        timestamped -> {
                            //Perform the scheduled activity here
//                            System.out.println("Out");

                            try {
                                Utility.popFromListToApi();
                            } catch (Exception ex) {
                                Logger.getLogger(MainVerticle.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        },
                        fault -> {
                            //If there is some kind of fault, handle here
                        }
                );
        vertx.createHttpServer().requestHandler(router::accept).listen(8080);

    }

}
