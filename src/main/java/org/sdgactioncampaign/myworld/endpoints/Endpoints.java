/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sdgactioncampaign.myworld.endpoints;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;
import java.io.UnsupportedEncodingException;
import org.sdgactioncampaign.myworld.Utility;

/**
 *
 * @author dipo
 */
public class Endpoints {

    public void handleAddSurvey(RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();
        try {

            String request = routingContext.getBodyAsString();
            new Utility().processSurveys(Utility.splitQuery(request)).setHandler(hndlr -> {
                if (hndlr.succeeded()) {
                    response.setStatusCode(200).end();
                }
                if (hndlr.failed()) {
                    response.setStatusCode(500).end();
                }
            });

        } catch (UnsupportedEncodingException ex) {
            response.setStatusCode(500).end();
        }

    }
}
