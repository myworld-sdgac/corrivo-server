package org.sdgactioncampaign.myworld;

import io.vertx.core.DeploymentOptions;
import org.sdgactioncampaign.myworld.MainVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.sdgactioncampaign.myworld.Utility;

@RunWith(VertxUnitRunner.class)
public class MainVerticleTest {

    private Vertx vertx;
    JsonObject config;

    @Before
    public void setUp(TestContext tc) {
        vertx = Vertx.vertx();
        DeploymentOptions deploymentOptions = new DeploymentOptions().setWorker(true);
        if (deploymentOptions.getConfig() == null) {
            deploymentOptions.setConfig(new JsonObject());
        }

        File conf = new File("src/conf/config.json");
        deploymentOptions.getConfig().mergeIn(Utility.getConfiguration(conf));
        vertx.deployVerticle(MainVerticle.class.getName(), deploymentOptions, tc.asyncAssertSuccess());
    }

    @After
    public void tearDown(TestContext tc) {
        vertx.close(tc.asyncAssertSuccess());
    }

    @Test
    public void testThatTheServerIsStarted(TestContext tc) {
        Async async = tc.async();
        vertx.createHttpClient().getNow(8080, "localhost", "/", response -> {
            tc.assertEquals(response.statusCode(), 200);
            System.out.println("=====> HTTP Server OK <=====");
            response.bodyHandler(body -> {
                tc.assertTrue(body.length() > 0);
                async.complete();
            });
        });
    }

    @Test
    public void testThatRedisIsOn(TestContext tc) {
        Async async = tc.async();
        Utility.redis.ping(pong -> {
            tc.assertEquals("PONG", pong.result());
            System.out.println("=====> Redis server ON <=====");
            async.complete();
        });
    }

    @Test
    public void checkSurveyLengthTest(TestContext tc) {
        Async async = tc.async();
        Utility.redis.llen("surveys", hndlr -> {
            tc.assertTrue(hndlr.result() instanceof Long);
            System.out.println("=====> checkSurveyLength OK <=====");
            async.complete();
        });
    }

    @Test
    public void splitQueryTest(TestContext tc) {
        Map<String, String> correct = new HashMap();
        correct.put("a", "1");
        correct.put("b", "2");
        correct.put("c", "3");
        try {
            Map<String, String> test = Utility.splitQuery("a=1&b=2&c=3");
            tc.assertEquals(correct, test);
            System.out.println("=====> splitQuery OK <=====");
        } catch (UnsupportedEncodingException ex) {
            tc.fail();
        }

    }

    @Test
    public void convertSurveysToQueryStringTest(TestContext tc) {
        String key = Utility.config.getJsonObject("apiParam").getString("key");
        String lng = Utility.config.getJsonObject("apiParam").getString("lng");
        String soruce = Utility.config.getJsonObject("apiParam").getString("soruce");
        String correct = "key=" + key + "&q1=yes&gender=&age=&disability=&disability_type=&edulevel=&country=NG&city=&partnerid=&userid=&lng=" + lng + "&goal_1=5&goal_2=4&goal_3=3&goal_4=2&goal_5=5&goal_6=1&goal_7=&goal_8=&goal_9=&goal_10=&goal_11=&goal_12=&goal_13=&goal_14=&goal_15=&goal_16=&goal_17=&mac_address=&phone_number=+2347036708627&soruce=" + soruce;
        Map<String, String> surveyMap = new HashMap();
        surveyMap.put("FromCountry", "NG");
        surveyMap.put("FromCity", "");
        surveyMap.put("From", "+2347036708627");
        surveyMap.put("Body", "YA5B4C3D2E5F1");
        tc.assertTrue(correct.equals(Utility.convertSurveysToQueryString("YA5B4C3D2E5F1", surveyMap, Utility.config)));

        System.out.println("=====> convertSurveysToQueryString OK <=====");

    }

    @Test(expected = NullPointerException.class)
    public void convertSurveysToQueryStringNullTest(TestContext tc) {
        Utility.convertSurveysToQueryString(null, null, null);
    }

}
